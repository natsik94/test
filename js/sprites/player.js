import { Sprite, Texture } from 'pixi.js';
import { getTexture } from '../common/assets';
import { appConstants } from '../common/constants';
import { allTexturesKeys } from '../common/textures';

let player,
		app,
		mainTexture,
		rightTexture;

export const addPlayer = (currApp, root) => {
	if (player) {
		return player;
	}

	app = currApp;
	player = new Sprite(getTexture(allTexturesKeys.cat));
	player.name = appConstants.containers.player;
	player.anchor.set(0.5);
	
	player.position.x = app.screen.width / 2;
	player.position.y = app.screen.height - 134;

	 mainTexture = getTexture(allTexturesKeys.cat);
	 rightTexture = getTexture(allTexturesKeys.catRight)

	return player;
};

export const getPlayer = () => player;

export const playerTick = (state) => {
	const playerPosition = player.position.x;
	player.position.x = state.mousePosition < 150 ? 150 : state.mousePosition;

	if (player.position.x < playerPosition) {
		player.texture = mainTexture;
	} else if (player.position.x > playerPosition) {
		player.texture = rightTexture;
	} else {
		player.texture = mainTexture;
	}
};