export const appConstants = {
	size: {
		WIDTH: window.innerWidth ?? 800,
		HEIGHT: window.innerHeight ?? 600,
	},
	containers: {
		player: 'player',
		wall: 'wall',
	}
}