export const allTexturesKeys = {
	wall: 'wall',
	cat: 'cat',
	catRight: 'catRight',
};

export const appTextures = {
	[allTexturesKeys.wall]: 'assets/objects/wall.jpg',
	[allTexturesKeys.cat]: 'assets/unit/cat.png',
	[allTexturesKeys.catRight]: 'assets/unit/cat-right.png',
}