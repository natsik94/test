import { Sprite } from 'pixi.js';
import { getTexture } from '../common/assets';
import { appConstants } from '../common/constants';
import { allTexturesKeys } from '../common/textures';

let wall,
		app;

export const addWall = (currApp, root) => {
	if (wall) {
		return wall;
	}

	app = currApp;

	for (let i = 0; i < (appConstants.size.WIDTH / 100 + 1); i++) {
		wall = new Sprite(getTexture(allTexturesKeys.wall));

		wall.x = i * 100;
		wall.y = 0;
		root.addChild(wall);

		const wallBottom = new Sprite(getTexture(allTexturesKeys.wall));

		wallBottom.x = i * 100;
		wallBottom.y = app.screen.height - 100;
		root.addChild(wallBottom);
	}

	for (let i = 0; i < (appConstants.size.HEIGHT / 100 + 1); i++) {
		const wall = new Sprite(getTexture(allTexturesKeys.wall));

		wall.x = 0;
		wall.y = i * 100;
		root.addChild(wall);
	}
};